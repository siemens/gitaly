package raft

import (
	"context"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitaly/v16/internal/git/gittest"
	"gitlab.com/gitlab-org/gitaly/v16/internal/gitaly/config"
	"gitlab.com/gitlab-org/gitaly/v16/internal/gitaly/service"
	"gitlab.com/gitlab-org/gitaly/v16/internal/gitaly/storage/keyvalue"
	"gitlab.com/gitlab-org/gitaly/v16/internal/gitaly/storage/keyvalue/databasemgr"
	"gitlab.com/gitlab-org/gitaly/v16/internal/gitaly/storage/node"
	"gitlab.com/gitlab-org/gitaly/v16/internal/gitaly/storage/raftmgr"
	"gitlab.com/gitlab-org/gitaly/v16/internal/gitaly/storage/storagemgr"
	"gitlab.com/gitlab-org/gitaly/v16/internal/helper"
	"gitlab.com/gitlab-org/gitaly/v16/internal/log"
	"gitlab.com/gitlab-org/gitaly/v16/internal/testhelper"
	"gitlab.com/gitlab-org/gitaly/v16/internal/testhelper/testcfg"
	"gitlab.com/gitlab-org/gitaly/v16/internal/testhelper/testserver"
	"gitlab.com/gitlab-org/gitaly/v16/proto/go/gitalypb"
	"go.etcd.io/etcd/raft/v3/raftpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	clusterID      = "test-cluster"
	authorityName  = "test-authority"
	storageNameOne = "default"
	storageNameTwo = "default-two"
)

func TestServer_SendMessage(t *testing.T) {
	t.Parallel()

	ctx := testhelper.Context(t)
	cfg := testcfg.Build(t, testcfg.WithStorages(storageNameOne, storageNameTwo))
	cfg.Raft.ClusterID = clusterID
	logger := testhelper.SharedLogger(t)

	// Create unique directory for database
	dbPath := testhelper.TempDir(t)
	dbMgr, err := databasemgr.NewDBManager(
		ctx,
		cfg.Storages,
		func(logger log.Logger, path string) (keyvalue.Store, error) {
			return keyvalue.NewBadgerStore(logger, filepath.Join(dbPath, path))
		},
		helper.NewNullTickerFactory(),
		logger,
	)
	require.NoError(t, err)
	t.Cleanup(dbMgr.Close)

	metrics := storagemgr.NewMetrics(cfg.Prometheus)
	nodeMgr, err := node.NewManager(cfg.Storages, storagemgr.NewFactory(logger, dbMgr, nil, 2, metrics))
	require.NoError(t, err)
	t.Cleanup(nodeMgr.Close)

	mockNode, err := raftmgr.NewNode(cfg, nodeMgr, log.LogrusLogger{}, dbMgr, nil)
	require.NoError(t, err)

	// Register storage one
	storage, err := mockNode.GetStorage(storageNameOne)
	require.NoError(t, err)

	registry := storage.(*raftmgr.RaftStorageWrapper).GetManagerRegistry()
	raftMgr := &mockRaftManager{}

	partitionKey := &gitalypb.PartitionKey{
		AuthorityName: authorityName,
		PartitionId:   1,
	}
	err = registry.RegisterManager(partitionKey, raftMgr)
	require.NoError(t, err)

	// Register storage two
	storageTwo, err := mockNode.GetStorage(storageNameTwo)
	require.NoError(t, err)

	registryTwo := storageTwo.(*raftmgr.RaftStorageWrapper).GetManagerRegistry()
	raftMgrTwo := &mockRaftManager{}
	err = registryTwo.RegisterManager(partitionKey, raftMgrTwo)
	require.NoError(t, err)

	client := runRaftServer(t, ctx, cfg, mockNode)

	testCases := []struct {
		desc            string
		req             *gitalypb.RaftMessageRequest
		expectedGrpcErr codes.Code
		expectedError   string
	}{
		{
			desc: "successful message send to storage one",
			req: &gitalypb.RaftMessageRequest{
				ClusterId: "test-cluster",
				ReplicaId: &gitalypb.ReplicaID{
					StorageName: storageNameOne,
					PartitionKey: &gitalypb.PartitionKey{
						AuthorityName: authorityName,
						PartitionId:   1,
					},
				},
				Message: &raftpb.Message{
					Type: raftpb.MsgApp,
					To:   2,
				},
			},
		},
		{
			desc: "successful message send to storage two",
			req: &gitalypb.RaftMessageRequest{
				ClusterId: "test-cluster",
				ReplicaId: &gitalypb.ReplicaID{
					StorageName: storageNameTwo,
					PartitionKey: &gitalypb.PartitionKey{
						AuthorityName: authorityName,
						PartitionId:   1,
					},
				},
				Message: &raftpb.Message{
					Type: raftpb.MsgApp,
					To:   2,
				},
			},
		},
		{
			desc: "missing cluster ID",
			req: &gitalypb.RaftMessageRequest{
				ReplicaId: &gitalypb.ReplicaID{
					StorageName: "storage-name",
					PartitionKey: &gitalypb.PartitionKey{
						AuthorityName: authorityName,
						PartitionId:   1,
					},
				},
				Message: &raftpb.Message{
					Type: raftpb.MsgApp,
					To:   2,
				},
			},
			expectedGrpcErr: codes.InvalidArgument,
			expectedError:   "rpc error: code = InvalidArgument desc = cluster_id is required",
		},
		{
			desc: "wrong cluster ID",
			req: &gitalypb.RaftMessageRequest{
				ClusterId: "wrong-cluster",
				ReplicaId: &gitalypb.ReplicaID{
					StorageName: "storage-name",
					PartitionKey: &gitalypb.PartitionKey{
						AuthorityName: authorityName,
						PartitionId:   1,
					},
				},
				Message: &raftpb.Message{
					Type: raftpb.MsgApp,
					To:   2,
				},
			},
			expectedGrpcErr: codes.PermissionDenied,
			expectedError:   `rpc error: code = PermissionDenied desc = message from wrong cluster: got "wrong-cluster", want "test-cluster"`,
		},
		{
			desc: "missing authority name",
			req: &gitalypb.RaftMessageRequest{
				ClusterId: "test-cluster",
				ReplicaId: &gitalypb.ReplicaID{
					StorageName: storageNameOne,
					PartitionKey: &gitalypb.PartitionKey{
						PartitionId: 1,
					},
				},
				Message: &raftpb.Message{
					Type: raftpb.MsgApp,
					To:   2,
				},
			},
			expectedGrpcErr: codes.InvalidArgument,
			expectedError:   "rpc error: code = InvalidArgument desc = authority_name is required",
		},
		{
			desc: "missing partition ID",
			req: &gitalypb.RaftMessageRequest{
				ClusterId: "test-cluster",
				ReplicaId: &gitalypb.ReplicaID{
					StorageName: storageNameOne,
					PartitionKey: &gitalypb.PartitionKey{
						AuthorityName: authorityName,
					},
				},
				Message: &raftpb.Message{
					Type: raftpb.MsgApp,
					To:   2,
				},
			},
			expectedGrpcErr: codes.InvalidArgument,
			expectedError:   "rpc error: code = InvalidArgument desc = partition_id is required",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			t.Parallel()

			stream, err := client.SendMessage(ctx)
			require.NoError(t, err)

			require.NoError(t, stream.Send(tc.req))

			_, err = stream.CloseAndRecv()
			if tc.expectedGrpcErr == codes.OK {
				require.NoError(t, err)
			} else {
				testhelper.RequireGrpcCode(t, err, tc.expectedGrpcErr)
				require.Contains(t, err.Error(), tc.expectedError)
			}
		})
	}
}

func runRaftServer(t *testing.T, ctx context.Context, cfg config.Cfg, node *raftmgr.Node) gitalypb.RaftServiceClient {
	serverSocketPath := testserver.RunGitalyServer(t, cfg, func(srv *grpc.Server, deps *service.Dependencies) {
		deps.Cfg = cfg
		deps.Node = node
		gitalypb.RegisterRaftServiceServer(srv, NewServer(deps))
	}, testserver.WithDisablePraefect())

	cfg.SocketPath = serverSocketPath

	conn := gittest.DialService(t, ctx, cfg)

	return gitalypb.NewRaftServiceClient(conn)
}
